(() => {
    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again,
     * it will do nothing next time.
     */
    if (window.hasRun) { return }
    window.hasRun = true;


    // check if can get location
    if (!navigator.geolocation) {
        console.error( "Cannot get geolocation" )
        return
    }

    // how often to check weather (every 10sec)
    const CHECK_WEATHER = 10000

    // how often to change website (every minutes)
    const CHECK_WEBSITE= 60000

    // weather api parameters
    const CLIMATE_API_KEY = 'f74916e1c9606ec06e53c7c0d82b6674'


    // setup style tag
    let style = set_up_weather_styles_tag()

    // every few seconds
    run();
    setInterval(() => run() , CHECK_WEATHER )
    setInterval(() => fetch_request() , CHECK_WEBSITE )

    // run
    function run() {
        // get location latitude and longitutde from browser
        navigator.geolocation.getCurrentPosition( position => {
            const lat = position.coords.latitude
            const lon = position.coords.longitude
            // construct api URL with current lat and lon info
            const api_url = `https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lon }&appid=${ CLIMATE_API_KEY }`
            // fetch weather data
            fetch( api_url )
                .then( response => response.json() )
                .then( data => set_weather_css( data ) )
                .catch( err => console.error( err ) )
        })
    }

    // get the pad where the website list is
    function fetch_request(){
        // fetching the pad
        let url = "https://pad.constantvzw.org/p/declarations-websites";
        fetch(`${url}/export/txt`, {
            'method': 'GET',
            'mode': 'cors',
        })
        .then(response => response.text() )
        .then(pad_text => refresh(pad_text) );
    }

    // refresh webpage
    function refresh(websites) {

        let list = websites.split("\n");
        
        // get the index we're currently on through local storage
        browser.storage.local.get('weather_index').then(function(result){
            if(result.weather_index){
                i = result.weather_index;
            }
            else{
                i = 0;
            }
        }).then(function(){
            let go_to = list[i];
            i = i + 1;
            if(i >= list.length){
                i = 0;
            }
            console.log(go_to);
            browser.storage.local.set({ "weather_index": i });
            if(go_to.startsWith("http")){
                window.location.href = go_to;
            }
        });
    }

    // set up styles tag
    function set_up_weather_styles_tag() {
        const style = document.createElement( 'style' )
        style.id = "weather_style"
        style.setAttribute( 'contenteditable', true )
        document.body.appendChild( style )
        return style
    }


    // set wind data
    function set_weather_css( data ) {
        // console.log( data )
        const wind = blow( data.wind.speed )
        // const humidity = moisten( data.main.humidity )
        // const temp = temperature( data.main.temp )
        style.innerHTML = `
            /* wind */
            ${ wind }
            /* humidity */
            /* temperature */
            /* cloudiness */
            /* precipitation */
        `
    }

    let console_style = {
        "wind_blows": "height:150px; writing-mode:vertical-lr; text-orientation:upright; font-size: 20px; margin: 30px",
        "humid": "color:blue; font-size:15px; padding:145px; margin:20px; background:transparent; display:flex; border-radius:72% 28% 56% 44% / 30% 30% 70% 70%; box-shadow: inset 10px 10px 10px rgba(0,0,0,0.05), 15px 25px 10px rgba(0,0,0,0.1), 15px 20px 20px rgba(0,0,0,0.05), inset -10px -10px 15px rgba(255,255,255,0.9);",
        "temp": "color:white; text-shadow: #FC0 1px 0 20px; height:350px; display:flex; justify-content:center; align-items:center;  background-image:linear-gradient(yellow,blue); border-radius:50%; ",
    }

    function weather_mood (text, style_id) {
        var string = "%c" + text;
        console.log(string, console_style[style_id]);
    }


    // function to blow website
    function blow( wind ) {
        weather_mood(`Blowing at local windspeed ${ wind }m/s.`, "wind_blows")
        wind /= 5
        return `
            body * {
                transform: rotate(${ Math.random() * wind - wind / 2 }deg);
                transition: transform 5000ms ease;
            }
        `
    }

    // function to handle humidity
    function moisten( humidity ) {
        console.log(`Setting humidity to ${ humidity }%.`)
        humidity *= 0.01
        return `
            body::after{
                /* overlay something above everything */
                position: fixed;
                inset: 0;
                content: "";
                pointer-events: none;
                z-index: 999999;

                /* make the edges white and center visible */
                background: radial-gradient(closest-side, transparent ${ 100 * humidity }%, white);

                /* make everything below blurry - value can be modified */
                backdrop-filter: blur(${ humidity }px);
                /* but still show a bit of the original non-blured - value can be modified */
                opacity: ${ humidity };
                transition: all 1000ms linear;
            }
        `
    }

    // temperature
    function temperature( temperature ) {
        temperature -= 273.15
        console.log( `Setting temperature to ${ temperature }℃.` )
        if ( temperature > 10 ) {
            return `
                /* melting (for above a certain level of temperature) */
                p {
                    --melting-strongness: ${ temperature };
                    position: relative;
                    transform-origin: top center;
                    animation: melt 5s linear infinite;
                }
                @keyframes melt{
                    0%{top: 0; opacity: 0; transform: scaleY(1);}
                    10%{top: 0; opacity: 1; transform: scaleY(1);}
                    55%{top: calc(0.1em * var(--melting-strongness)); opacity: 1; transform: scaleY(calc(1 + (0.05 * var(--melting-strongness))));}
                    100%{top: calc(0.2em * var(--melting-strongness)); opacity: 0; transform: scaleY(calc(1 + (0.1 * var(--melting-strongness))));}
                }
            `
        } else {
            return ``
        }

    }

    /**
     * Listen to popup message
     */
    browser.runtime.onMessage.addListener((message) => {

        // if it get that message!
        if (message.command === "blow") {
            blow( message.value );
        }

    });

})();
